$(document).ready(() => {
    $("a.about-us").click(() => {
        $("article.about-us").removeClass("hidden");
        $("article.asset-care").addClass("hidden");
    });
    $("a.asset-care").click(() => {
        $("article.about-us").addClass("hidden");
        $("article.asset-care").removeClass("hidden");
    });
    $("form").submit(() => {
        let name = $("input#name").val();
        let body = $("textarea#message").val();
        let subject = `Fidelia Assets Contact - ${name}`;

        window.location.href = `mailto:admin@fideliaassets.com?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(body)}`;
        return false;
    });
});